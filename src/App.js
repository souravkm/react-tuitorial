import React, { useState } from 'react';


import { Transition } from 'react-transition-group';

const AComponent = ({ in: inProp }) => (
  <Transition
    in={inProp}
    timeout={{
      appear: 100,
      enter: 300,
      exit: 300
    }}
    appear
    unmountOnExit
  >
    {state => (
      <div
        style={{
          ...defaultStyle,
          ...transitionStyles[state]
        }}
      >
        I am {state}
      </div>
    )}
  </Transition>
);
 

function App(){
  const [entered, setEntered] = useState(false);
  return (
    <div
>
      <AComponent in={entered} />
        <button
          onClick={() => {
            setEntered(!entered);
          }}
        >
          Toggle Entered
        </button>
    </div>
  );
}

const defaultStyle = {
  transition: `transform 200ms, opacity 200ms ease`,
  opacity: 1
};

const transitionStyles = {
  entering: { transform: 'scale(0.5)', opacity: 0 },
  entered: { transform: 'scale(2.0)', opacity: 1},
  exiting: { opacity: 0 },
  exited: { opacity: 0 }
};


export default App;
